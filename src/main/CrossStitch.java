package main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import helper.ColorReducer;
import helper.ImageCreator;
import helper.TranslateImage;

import model.Model;

public class CrossStitch{

	public static void main(String[] args){
		String input = "default.png";
		if(args.length < 2){
			System.out.println();
			System.out.println("Usage:");
			System.out.println("\targ[0]: reduced number of colors");
			System.out.println("\targ[1]: output file");
			System.out.println("\targ[2]: (optional) input file (default is \"default.png\")");
			System.out.println();
			return;
		}
		if(args.length > 2){
			input = args[2];
		}
		new CrossStitch(Integer.valueOf(args[0]), args[1], input);
	}

	public CrossStitch(int numberOfColors, String output, String input){
		try{
			BufferedImage image = ImageIO.read(new File("input/" + input));
			Model.getInstance().setHeight(image.getHeight());
			Model.getInstance().setWidth(image.getWidth());
			int[][] grid = new int[image.getHeight()][image.getWidth()];
			for(int i = 0; i < image.getHeight(); ++i){
				for(int j = 0; j < image.getWidth(); ++j){
					grid[i][j] = image.getRGB(j, i);
				}
			}
			Model.getInstance().setImage(grid);
			new TranslateImage();
			new ColorReducer(numberOfColors);
			new ImageCreator(output);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}
