package helper;

import java.awt.Color;
import java.lang.Math;
import java.util.HashMap;
import java.util.Map;

import model.Model;

public class ColorReducer{

	private int[][] image;
	private Map<Color, Integer> colors;

	public ColorReducer(int numberOfColors){
		this.colors = new HashMap<Color, Integer>();
		this.image = Model.getInstance().getImage();
		while(this.removeColor(numberOfColors) > 0){}
		Model.getInstance().setImage(this.image);
	}

	private void countColors(){
		this.colors.clear();
		for(int i = 0; i < this.image.length; ++i){
			for(int j = 0; j < this.image[i].length; ++j){
				Color c = new Color(this.image[i][j]);
				int count = 0;
				if(this.colors.containsKey(c)){
					count = this.colors.get(c);
					this.colors.remove(c);
				}
				++count;
				this.colors.put(c, count);
			}
		}
	}

	private int removeColor(int numberOfColors){
		this.countColors();
		if(this.colors.size() <= numberOfColors){
			return 0;
		}

		int min = -1;
		Color dead = null;
		for(Color c : this.colors.keySet()){
			if(dead == null || this.colors.get(c) < min){
				dead = c;
				min = this.colors.get(c);
			}
		}

		Color closest = null;
		double best = -1.0;
		for(Color c : this.colors.keySet()){
			if(c == dead){
				continue;
			}
			double diff = this.getDifference(dead, c);
			if(closest == null || diff < best){
				closest = c;
				best = diff;
			}
		}

		for(int i = 0; i < this.image.length; ++i){
			for(int j = 0; j < this.image[i].length; ++j){
				int c = this.image[i][j];
				if(this.image[i][j] == dead.getRGB()){
					this.image[i][j] = closest.getRGB();
				}
			}
		}

		Model.getInstance().setImage(this.image);

		return 1;
	}

	private double getDifference(Color c1, Color c2){
		int r1 = c1.getRed();
		int g1 = c1.getGreen();
		int b1 = c1.getBlue();
		int r2 = c2.getRed();
		int g2 = c2.getGreen();
		int b2 = c2.getBlue();

		return Math.sqrt(Math.pow(r2 - r1, 2) + Math.pow(g2 - g1, 2) + Math.pow(b2 - b1, 2));
	}
}
