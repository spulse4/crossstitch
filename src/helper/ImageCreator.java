package helper;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import model.Model;

public class ImageCreator{

	public ImageCreator(String output){
		int[][] raw = Model.getInstance().getImage();
		BufferedImage image = new BufferedImage(raw[0].length, raw.length,
			BufferedImage.TYPE_INT_RGB);
		for(int i = 0; i < raw.length; ++i){
			for(int j = 0; j < raw[i].length; ++j){
				image.setRGB(j, i, raw[i][j]);
			}
		}
		File out = new File("output/" + output);
		try{
			ImageIO.write(image, "png", out);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}
