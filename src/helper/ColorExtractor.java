package helper;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import model.Thread;

//extracts colors from constants files of RGB threads
public class ColorExtractor{

	private static ColorExtractor instance = null;

	public static ColorExtractor getInstance(){
		if(instance == null){
			instance = new ColorExtractor();
		}
		return instance;
	}

	private List<Thread> threads;

	private ColorExtractor(){
		this.threads = new ArrayList<Thread>();
		try{
			File input = new File("constants/dmcColors.csv");
			Scanner scanner = new Scanner(input);

			while(scanner.hasNextLine()){
				String line = scanner.nextLine();
				if(line.equals("")){
					continue;
				}
				Scanner lineScanner = new Scanner(line);
				lineScanner.useDelimiter(",");

				String name = lineScanner.next();
				int id = Integer.valueOf(lineScanner.next());
				int r = Integer.valueOf(lineScanner.next());
				int g = Integer.valueOf(lineScanner.next());
				int b = Integer.valueOf(lineScanner.next());

				threads.add(new Thread(name, id, r, g, b));

				lineScanner.close();
			}

			scanner.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}

	public int match(int rgb){
		Thread closest = null;
		double min = -1.0;
		for(Thread t : this.threads){
			double diff = this.getDifference(rgb, t.getColor());
			if(closest == null || diff < min){
				closest = t;
				min = diff;
			}
		}
		return closest.getColor();
	}

	private double getDifference(int rgb1, int rgb2){
		Color c1 = new Color(rgb1);
		Color c2 = new Color(rgb2);
		int r1 = c1.getRed();
		int g1 = c1.getGreen();
		int b1 = c1.getBlue();
		int r2 = c2.getRed();
		int g2 = c2.getGreen();
		int b2 = c2.getBlue();

		return Math.sqrt(Math.pow(r2 - r1, 2) + Math.pow(g2 - g1, 2) + Math.pow(b2 - b1, 2));
	}
}
