package helper;

import helper.ColorExtractor;

import model.Model;

public class TranslateImage{

	private int[][] image;

	public TranslateImage(){
		this.image = Model.getInstance().getImage();

		for(int i = 0; i < this.image.length; ++i){
			for(int j = 0; j < this.image[i].length; ++j){
				this.image[i][j] = ColorExtractor.getInstance().match(this.image[i][j]);
			}
		}

		Model.getInstance().setImage(this.image);
	}
}
