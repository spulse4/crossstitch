package model;

public class Model{

	private static Model instance = null;

	public static Model getInstance(){
		if(instance == null){
			instance = new Model();
		}
		return instance;
	}

	private int height;
	private int width;
	private int[][] image;

	private Model(){}

	public void setHeight(int height){
		this.height = height;
	}
	public void setWidth(int width){
		this.width = width;
	}
	public void setImage(int[][] image){
		this.image = image;
	}
	public int[][] getImage(){
		return this.image;
	}
}
