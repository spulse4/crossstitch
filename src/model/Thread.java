package model;

import java.awt.Color;

public class Thread{

	private int id;
	private int red;
	private int green;
	private int blue;
	private String name;

	public Thread(String name, int id, int red, int green, int blue){
		this.name = name;
		this.id = id;
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	public int getColor(){
		return new Color(this.red, this.green, this.blue).getRGB();
	}
}
